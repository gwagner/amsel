/* amsel-feedly.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "amsel-feedly.h"
#include "amsel-feed-provider.h"
#include <libpeas/peas.h>

struct _AmselFeedly
{
  GObject parent_instance;
};

static void amsel_feedly_feed_provider_init (AmselFeedProviderInterface *iface);
G_DEFINE_TYPE_WITH_CODE (AmselFeedly, amsel_feedly, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (AMSEL_TYPE_FEED_PROVIDER, amsel_feedly_feed_provider_init))

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

AmselFeedly *
amsel_feedly_new (void)
{
  return g_object_new (AMSEL_TYPE_FEEDLY, NULL);
}

static void
amsel_feedly_finalize (GObject *object)
{
  AmselFeedly *self = (AmselFeedly *)object;

  G_OBJECT_CLASS (amsel_feedly_parent_class)->finalize (object);
}

static void
amsel_feedly_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  AmselFeedly *self = AMSEL_FEEDLY (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
amsel_feedly_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  AmselFeedly *self = AMSEL_FEEDLY (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
amsel_feedly_class_init (AmselFeedlyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = amsel_feedly_finalize;
  object_class->get_property = amsel_feedly_get_property;
  object_class->set_property = amsel_feedly_set_property;
}

static void
amsel_feedly_init (AmselFeedly *self)
{
}

static void
amsel_feedly_load (AmselFeedProvider *provider)
{

}

static const gchar *
amsel_feedly_get_name (AmselFeedProvider *provider)
{
  return "Feedly";
}

static GdkPixbuf *
amsel_feedly_get_icon (AmselFeedProvider *provider)
{
  return NULL;
}

static AmselChannel *
amsel_feedly_retrieve (AmselFeedProvider *provider,
                       const gchar       *url)
{
  return NULL;
}

static void
amsel_feedly_feed_provider_init (AmselFeedProviderInterface *iface)
{
  iface->load = amsel_feedly_load;
  iface->get_name = amsel_feedly_get_name;
  iface->get_icon = amsel_feedly_get_icon;
  iface->retrieve = amsel_feedly_retrieve;
}

void
peas_register_types (PeasObjectModule *module)
{
  peas_object_module_register_extension_type (module, AMSEL_TYPE_FEED_PROVIDER, AMSEL_TYPE_FEEDLY);
}
