/* amsel-feed-provider-manager.h
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

#include <glib-object.h>
#include <amsel-feed-provider.h>

G_BEGIN_DECLS

#define AMSEL_TYPE_FEED_PROVIDER_MANAGER (amsel_feed_provider_manager_get_type())

G_DECLARE_FINAL_TYPE (AmselFeedProviderManager, amsel_feed_provider_manager, AMSEL, FEED_PROVIDER_MANAGER, GObject)

AmselFeedProviderManager *amsel_feed_provider_manager_new (void);
GList *amsel_feed_provider_manager_get_provider_list (AmselFeedProviderManager *self);
AmselFeedProvider *amsel_feed_provider_manager_get_provider (AmselFeedProviderManager *self,
                                                             const gchar              *providername);
void amsel_feed_provider_manager_add_provider (AmselFeedProviderManager *self,
                                               const gchar              *provider_name,
                                               AmselFeedProvider        *provider);
G_END_DECLS
