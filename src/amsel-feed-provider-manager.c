/* amsel-feed-provider-manager.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include "amsel-feed-provider-manager.h"

/**
 * SECTION:amsel-feed-provider-manager
 * @title: AmselFeedProviderManager
 * @see_also: #AmselFeedProvider
 *
 * Every provider plugin gets managed via this manager. These plugins have
 * a module name which acts as identifier to find them.
 */

struct _AmselFeedProviderManager
{
  GObject parent_instance;

  GHashTable *provider;
};

G_DEFINE_TYPE (AmselFeedProviderManager, amsel_feed_provider_manager, G_TYPE_OBJECT)

/**
 * amsel_feed_provider_manager_new:
 *
 * Create a new #AmselFeedProviderManager.
 *
 * Returns: the new #AmselFeedProviderManager
 */
AmselFeedProviderManager *
amsel_feed_provider_manager_new (void)
{
  return g_object_new (AMSEL_TYPE_FEED_PROVIDER_MANAGER, NULL);
}

static void
amsel_feed_provider_manager_finalize (GObject *object)
{
  AmselFeedProviderManager *self = (AmselFeedProviderManager *)object;

  g_hash_table_unref (self->provider);

  G_OBJECT_CLASS (amsel_feed_provider_manager_parent_class)->finalize (object);
}

static void
amsel_feed_provider_manager_class_init (AmselFeedProviderManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = amsel_feed_provider_manager_finalize;
}

static void
amsel_feed_provider_manager_init (AmselFeedProviderManager *self)
{
  self->provider = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
}

/**
 * amsel_feed_provider_manager_get_provider_list:
 * @self: a #AmselFeedProviderManager
 *
 * get all #AmselFeedProvider which got loaded.
 *
 * Returns: (element-type AmselFeedProvider) (transfer container): the list of
 *          loaded #AmselFeedProvider. Free the #GList after usage with
 *          g_list_free.
 */
GList *
amsel_feed_provider_manager_get_provider_list (AmselFeedProviderManager *self)
{
  g_return_val_if_fail (AMSEL_IS_FEED_PROVIDER_MANAGER (self), NULL);

  return g_hash_table_get_keys (self->provider);
}

/**
 * amsel_feed_provider_manager_get_provider:
 * @self: a #AmselFeedProviderManager
 * @providername: the module name to identify the provider. See also
 *                #amsel_feed_provider_manager_add_provider
 *
 * Returns: (transfer full): the #AmselFeedProvider associated with the name
 */
AmselFeedProvider *
amsel_feed_provider_manager_get_provider (AmselFeedProviderManager *self,
                                          const gchar              *providername)
{
  AmselFeedProvider *provider;
  g_return_val_if_fail (AMSEL_IS_FEED_PROVIDER_MANAGER (self), NULL);
  g_return_val_if_fail (providername != NULL, NULL);

  provider = g_hash_table_lookup (self->provider, providername);

  return g_object_ref (provider);
}

/**
 * amsel_feed_provider_manager_add_provider:
 * @self: a #AmselFeedProviderManager
 * @provider_name: (transfer none): the name to identify this #AmselFeedProvider. This is normally
 *                 the module name.
 * @provider: (transfer full): the #AmselFeedProvider to add. The manager takes
 *                             ownership so don't free the provider
 *
 * adds the #AmselFeedProvider to the #AmselFeedProviderManager and executes
 * the provider loading mechanism
 */
void
amsel_feed_provider_manager_add_provider (AmselFeedProviderManager *self,
                                          const gchar              *provider_name,
                                          AmselFeedProvider        *provider)
{
  g_return_if_fail (AMSEL_IS_FEED_PROVIDER_MANAGER (self));
  g_return_if_fail (AMSEL_IS_FEED_PROVIDER (provider));

  amsel_feed_provider_load (provider);

  g_hash_table_insert (self->provider, g_strdup (provider_name), provider);
}
