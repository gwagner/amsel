/* amsel-application-window.c
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "amsel-application-window"

#include "amsel-application-window.h"
#include "amsel-application.h"
#include "amsel-feed-popover.h"
#include "amsel-util.h"
#include "amsel-debug.h"
#include <webkit2/webkit2.h>
#include <libsoup/soup.h>
#include "amsel-store.h"

struct _AmselApplicationWindow
{
  GtkApplicationWindow parent_instance;
  AmselStore *store;

  GtkStack *stack;
  GtkStack *stack_headerbar;
  GtkHeaderBar *headerbar_article;

  GtkButton *add_feed_btn;
  AmselFeedPopover *add_feed_popover;
  GtkViewport *entries_view;
  WebKitWebView *webview;

  GtkBox *box_channelprogress;
  GtkProgressBar *channelprogress;

  const gchar *article_link;
};

G_DEFINE_TYPE (AmselApplicationWindow, amsel_application_window, GTK_TYPE_APPLICATION_WINDOW)

static void
amsel_application_window_add_feed_clicked (GtkButton *btn,
                                           gpointer   user_data)
{
  AmselApplicationWindow *self = AMSEL_APPLICATION_WINDOW (user_data);

  if (gtk_widget_is_visible (GTK_WIDGET (self->add_feed_popover))) {
    gtk_popover_popdown (GTK_POPOVER (self->add_feed_popover));
  } else {
    gtk_popover_popup (GTK_POPOVER (self->add_feed_popover));
  }
}

static void
amsel_application_window_goto_link (GtkButton *btn,
                                    gpointer   user_data)
{
  AmselApplicationWindow *self = AMSEL_APPLICATION_WINDOW (user_data);

  gtk_show_uri_on_window (NULL, self->article_link, -1, NULL);
}

static void
amsel_application_window_go_back (GtkButton *btn,
                                  gpointer   user_data)
{
  AmselApplicationWindow *self = AMSEL_APPLICATION_WINDOW (user_data);

  gtk_stack_set_visible_child_name (self->stack, "list");
  gtk_stack_set_visible_child_name (self->stack_headerbar, "list");

}

static void
amsel_application_window_refresh (AmselEngine *engine,
                                  gint         num_channels,
                                  gpointer     user_data)
{
  AmselApplicationWindow *self = AMSEL_APPLICATION_WINDOW (user_data);

  g_autofree gchar *progresstext = g_strdup_printf ("%d / %d", 0, num_channels);
  gtk_progress_bar_set_text (self->channelprogress, progresstext);

  gtk_progress_bar_set_fraction (self->channelprogress, 0.0);
  gtk_widget_show (GTK_WIDGET (self->box_channelprogress));
}

static void
amsel_application_window_channel_fetched (AmselEngine  *engine,
                                          AmselChannel *channel,
                                          gpointer      user_data)
{
  AmselApplicationWindow *self = AMSEL_APPLICATION_WINDOW (user_data);

  // TODO: candidate to be optimized
  gint n_channels = amsel_engine_n_channels (engine);

  gdouble fraction = gtk_progress_bar_get_fraction (self->channelprogress);
  fraction += 1.0 / n_channels;
  gtk_progress_bar_set_fraction (self->channelprogress, fraction);

  if (fraction >= 1.0)
    gtk_widget_hide (GTK_WIDGET (self->box_channelprogress));

  const gchar *txt = gtk_progress_bar_get_text (self->channelprogress);
  if (txt != NULL) {
    gchar **txt_splitted = g_strsplit (txt, " / ", -1);
    gint n_channels_done = atoi (txt_splitted[0]);
    g_autofree gchar *progresstext = g_strdup_printf ("%d / %d", n_channels_done + 1, n_channels);
    gtk_progress_bar_set_text (self->channelprogress, progresstext);
  }

  g_object_unref (channel);
}

AmselApplicationWindow *
amsel_application_window_new (GtkApplication *app)
{
  return g_object_new (AMSEL_TYPE_APPLICATION_WINDOW,
                       "application", app,
                       "default-width", 1024,
                       "default-height", 768,
                       NULL);
}

static void
amsel_application_window_empty_cb (GListModel *list,
                                   guint       position,
                                   guint       removed,
                                   guint       added,
                                   gpointer    user_data)
{
  AmselApplicationWindow *self = AMSEL_APPLICATION_WINDOW (user_data);
  guint items = g_list_model_get_n_items (list);
  if (items == 0)
    {
      gtk_stack_set_visible_child_name (self->stack, "empty");
    }
  else
    {
      gtk_stack_set_visible_child_name (self->stack, "list");
    }
}

static void
amsel_application_window_constructed (GObject *object)
{
  AmselApplicationWindow *self = (AmselApplicationWindow *)object;
  AmselApplication *app = AMSEL_APPLICATION (g_application_get_default ());
  AmselEngine *engine = amsel_application_get_engine (app);
  AmselStore *store = amsel_application_get_store (app);
  gint n_channels;

  gtk_container_add (GTK_CONTAINER (self->entries_view), amsel_store_get_mainbox (store));
  self->add_feed_popover = amsel_feed_popover_new (engine);
  gtk_popover_set_relative_to (GTK_POPOVER (self->add_feed_popover), GTK_WIDGET (self->add_feed_btn));

  n_channels = amsel_engine_n_channels (engine);
  if (n_channels == 0)
    gtk_stack_set_visible_child_name (self->stack, "empty");

  GList * entries = amsel_engine_get_all_entries (engine);
  for (; entries; entries = g_list_next (entries))
    {
      amsel_store_append (store, entries->data);
    }
  gtk_stack_set_visible_child_name (self->stack, "list");

  g_signal_connect (engine, "channel-fetched", G_CALLBACK(amsel_application_window_channel_fetched), self);
  g_signal_connect (engine, "refresh", G_CALLBACK(amsel_application_window_refresh), self);

  G_OBJECT_CLASS (amsel_application_window_parent_class)->constructed (object);
}

static void
amsel_application_window_finalize (GObject *object)
{
  /* AmselApplicationWindow *self = (AmselApplicationWindow *)object; */

  G_OBJECT_CLASS (amsel_application_window_parent_class)->finalize (object);
}

static void
amsel_application_window_class_init (AmselApplicationWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = amsel_application_window_finalize;
  object_class->constructed = amsel_application_window_constructed;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Amsel/window.ui");
  gtk_widget_class_bind_template_child (widget_class, AmselApplicationWindow, add_feed_btn);
  gtk_widget_class_bind_template_child (widget_class, AmselApplicationWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, AmselApplicationWindow, stack_headerbar);
  gtk_widget_class_bind_template_child (widget_class, AmselApplicationWindow, entries_view);
  gtk_widget_class_bind_template_child (widget_class, AmselApplicationWindow, webview);
  gtk_widget_class_bind_template_child (widget_class, AmselApplicationWindow, box_channelprogress);
  gtk_widget_class_bind_template_child (widget_class, AmselApplicationWindow, channelprogress);
  gtk_widget_class_bind_template_child (widget_class, AmselApplicationWindow, headerbar_article);
  gtk_widget_class_bind_template_callback (widget_class, amsel_application_window_add_feed_clicked);
  gtk_widget_class_bind_template_callback (widget_class, amsel_application_window_go_back);
  gtk_widget_class_bind_template_callback (widget_class, amsel_application_window_goto_link);
}

static gboolean
amsel_application_window_decision_policy(WebKitWebView            *web_view,
                                         WebKitPolicyDecision     *decision,
                                         WebKitPolicyDecisionType  type)
{
  const gchar *uri;

  WebKitNavigationPolicyDecision *navigation_decision = WEBKIT_NAVIGATION_POLICY_DECISION (decision);
  WebKitNavigationAction *action = webkit_navigation_policy_decision_get_navigation_action (navigation_decision);
  WebKitURIRequest *request = webkit_navigation_action_get_request (action);

  uri = webkit_uri_request_get_uri (request);
  if (g_strcmp0 (uri, "about:blank") == 0)
    {
      return TRUE;
    }

  webkit_policy_decision_ignore (decision);

  gtk_show_uri_on_window (NULL, uri, -1, NULL);

  return TRUE;
}

static void
amsel_application_window_init (AmselApplicationWindow *self)
{
  g_type_ensure (WEBKIT_TYPE_WEB_VIEW);
  gtk_widget_init_template (GTK_WIDGET (self));

  g_signal_connect (self->webview, "decide-policy", G_CALLBACK (amsel_application_window_decision_policy), NULL);
}

void
amsel_application_window_show_article (AmselApplicationWindow *self,
                                       AmselEntry             *entry)
{
  g_return_if_fail (AMSEL_IS_APPLICATION_WINDOW (self));
  g_return_if_fail (WEBKIT_IS_WEB_VIEW (self->webview));
  AmselApplication *app = AMSEL_APPLICATION (g_application_get_default ());

  g_autofree gchar *html = g_strdup_printf ("<html><head><style>img { max-width: 100%%; height: auto; margin: auto; display: block; }</style></head><body style=\"max-width: 740px; margin: 0 auto; margin-top: 20px;\">%s</body></html>", amsel_entry_get_content (entry));
  webkit_web_view_load_html (self->webview, html, NULL);
  gtk_header_bar_set_title (self->headerbar_article, amsel_entry_get_title (entry));
  gtk_header_bar_set_subtitle (self->headerbar_article, amsel_entry_get_author (entry));
  self->article_link = amsel_entry_get_link (entry);

  AmselEngine *engine = amsel_application_get_engine (app);
  amsel_engine_mark_read (engine, entry, TRUE);

  gtk_stack_set_visible_child_name (self->stack, "article");
  gtk_stack_set_visible_child_name (self->stack_headerbar, "article");
}
