/* amsel-test.h
 *
 * Copyright 2018 Guenther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include <model/amsel-channel.h>
#include <gio/gio.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

G_BEGIN_DECLS

#define AMSEL_TYPE_FEED_PROVIDER (amsel_feed_provider_get_type ())

G_DECLARE_INTERFACE (AmselFeedProvider, amsel_feed_provider, AMSEL, FEED_PROVIDER, GObject)

struct _AmselFeedProviderInterface
{
  GTypeInterface parent;

  void          (*load)     (AmselFeedProvider *self);

  AmselChannel *(*retrieve) (AmselFeedProvider *self,
                             const gchar       *url);

  const gchar  *(*get_name) (AmselFeedProvider *self);

  GdkPixbuf    *(*get_icon) (AmselFeedProvider *self);
};

void            amsel_feed_provider_load        (AmselFeedProvider *self);
AmselChannel   *amsel_feed_provider_retrieve    (AmselFeedProvider *self,
                                                 const gchar       *url);

void            amsel_feed_provider_retrieve_async (AmselFeedProvider   *self,
                                                    const gchar         *url,
                                                    GCancellable        *cancellable,
                                                    GAsyncReadyCallback  callback,
                                                    gpointer             user_data);
AmselChannel   *amsel_feed_provider_retrieve_finish (AmselFeedProvider *self,
                                                     GAsyncResult      *res,
                                                     GError            *error);

const gchar    *amsel_feed_provider_get_name        (AmselFeedProvider *self);
GdkPixbuf      *amsel_feed_provider_get_icon        (AmselFeedProvider *self);
G_END_DECLS
